CREATE DATABASE  IF NOT EXISTS `project_infinity` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `project_infinity`;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account` (
  `accid` int(11) NOT NULL AUTO_INCREMENT,
  `Username` text,
  `Password` text,
  `Salt` text,
  `IsGM` tinyint(1) DEFAULT NULL,
  `Cash` int(11) DEFAULT NULL,
  `Mileage` int(11) DEFAULT NULL,
  PRIMARY KEY (`accid`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'wk3r','b00f0f3fcca6f65417da276f6c44656c','uAhnATJD8s9IUUPC',1,500000,200000);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auctionstorageitem`
--

DROP TABLE IF EXISTS `auctionstorageitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auctionstorageitem` (
  `price` int(11) unsigned DEFAULT NULL,
  `amount` smallint(6) unsigned DEFAULT NULL,
  `PlayerID` int(11) DEFAULT NULL,
  `ItemID` smallint(6) DEFAULT NULL,
  `Level` tinyint(3) unsigned DEFAULT NULL,
  `RareType` tinyint(3) unsigned DEFAULT NULL,
  `AddOption` tinyint(3) unsigned DEFAULT NULL,
  `AddOption2` tinyint(3) unsigned DEFAULT NULL,
  `AddOption3` tinyint(3) unsigned DEFAULT NULL,
  `Option` smallint(6) DEFAULT NULL,
  `Option2` smallint(6) DEFAULT NULL,
  `Option3` smallint(6) DEFAULT NULL,
  `Name` text COLLATE utf8mb4_unicode_ci,
  `isEtc` tinyint(1) DEFAULT NULL,
  `AuctionID` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auctionstorageitem`
--

LOCK TABLES `auctionstorageitem` WRITE;
/*!40000 ALTER TABLE `auctionstorageitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `auctionstorageitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `craftskill`
--

DROP TABLE IF EXISTS `craftskill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `craftskill` (
  `DBItemID` int(11) NOT NULL AUTO_INCREMENT,
  `PlayerID` int(11) DEFAULT NULL,
  `ItemID` smallint(6) DEFAULT NULL,
  `Position` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`DBItemID`)
) ENGINE=InnoDB AUTO_INCREMENT=2167 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `craftskill`
--

LOCK TABLES `craftskill` WRITE;
/*!40000 ALTER TABLE `craftskill` DISABLE KEYS */;
/*!40000 ALTER TABLE `craftskill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gameoptions`
--

DROP TABLE IF EXISTS `gameoptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gameoptions` (
  `AccountID` int(11) NOT NULL AUTO_INCREMENT,
  `op1` tinyint(3) unsigned DEFAULT NULL,
  `op2` tinyint(3) unsigned DEFAULT NULL,
  `op3` tinyint(3) unsigned DEFAULT NULL,
  `op4` tinyint(3) unsigned DEFAULT NULL,
  `op5` tinyint(3) unsigned DEFAULT NULL,
  `op6` tinyint(3) unsigned DEFAULT NULL,
  `op7` tinyint(3) unsigned DEFAULT NULL,
  `op8` tinyint(3) unsigned DEFAULT NULL,
  `op9` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`AccountID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gameoptions`
--

LOCK TABLES `gameoptions` WRITE;
/*!40000 ALTER TABLE `gameoptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `gameoptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gameserversettings`
--

DROP TABLE IF EXISTS `gameserversettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gameserversettings` (
  `ID` tinyint(3) unsigned NOT NULL,
  `ChannelAmmount` tinyint(3) unsigned DEFAULT NULL,
  `StartPort` smallint(6) DEFAULT NULL,
  `Notice` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gameserversettings`
--

LOCK TABLES `gameserversettings` WRITE;
/*!40000 ALTER TABLE `gameserversettings` DISABLE KEYS */;
INSERT INTO `gameserversettings` VALUES (0,1,2000,'Welcome to WK3R-PI! I hope ya\'ll are having a grand time! ~Riuga');
/*!40000 ALTER TABLE `gameserversettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift`
--

DROP TABLE IF EXISTS `gift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gift` (
  `Sender` text,
  `Year` tinyint(3) unsigned DEFAULT NULL,
  `Month` tinyint(3) unsigned DEFAULT NULL,
  `Day` tinyint(3) unsigned DEFAULT NULL,
  `IsGM` tinyint(1) DEFAULT NULL,
  `DBItemID` int(11) NOT NULL AUTO_INCREMENT,
  `PlayerID` int(11) DEFAULT NULL,
  `ItemID` smallint(6) DEFAULT NULL,
  `Count` smallint(6) unsigned DEFAULT NULL,
  `Position` tinyint(3) unsigned DEFAULT NULL,
  `ItemType_Byte` tinyint(3) unsigned DEFAULT NULL,
  `Level` tinyint(3) unsigned DEFAULT NULL,
  `RareType` tinyint(3) unsigned DEFAULT NULL,
  `AddOption` tinyint(3) unsigned DEFAULT NULL,
  `AddOption2` tinyint(3) unsigned DEFAULT NULL,
  `AddOption3` tinyint(3) unsigned DEFAULT NULL,
  `Option` smallint(6) DEFAULT NULL,
  `Option2` smallint(6) DEFAULT NULL,
  `Option3` smallint(6) DEFAULT NULL,
  `Name` text,
  PRIMARY KEY (`DBItemID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift`
--

LOCK TABLES `gift` WRITE;
/*!40000 ALTER TABLE `gift` DISABLE KEYS */;
/*!40000 ALTER TABLE `gift` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `giftbox`
--

DROP TABLE IF EXISTS `giftbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `giftbox` (
  `OwnerID` int(11) DEFAULT NULL,
  `ID` int(11) DEFAULT NULL,
  `Sender` text,
  `Year` tinyint(3) unsigned DEFAULT NULL,
  `Month` tinyint(3) unsigned DEFAULT NULL,
  `Day` tinyint(3) unsigned DEFAULT NULL,
  `IsGM` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `giftbox`
--

LOCK TABLES `giftbox` WRITE;
/*!40000 ALTER TABLE `giftbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `giftbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild`
--

DROP TABLE IF EXISTS `guild`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `guild` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text,
  `Message` text,
  `LeaderID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild`
--

LOCK TABLES `guild` WRITE;
/*!40000 ALTER TABLE `guild` DISABLE KEYS */;
/*!40000 ALTER TABLE `guild` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item` (
  `DBItemID` int(11) NOT NULL AUTO_INCREMENT,
  `PlayerID` int(11) DEFAULT NULL,
  `ItemID` smallint(6) DEFAULT NULL,
  `Count` smallint(6) unsigned DEFAULT NULL,
  `Position` tinyint(3) unsigned DEFAULT NULL,
  `ItemType_Byte` tinyint(3) unsigned DEFAULT NULL,
  `Level` tinyint(3) unsigned DEFAULT NULL,
  `RareType` tinyint(3) unsigned DEFAULT NULL,
  `AddOption` tinyint(3) unsigned DEFAULT NULL,
  `AddOption2` tinyint(3) unsigned DEFAULT NULL,
  `AddOption3` tinyint(3) unsigned DEFAULT NULL,
  `Option` smallint(6) DEFAULT NULL,
  `Option2` smallint(6) DEFAULT NULL,
  `Option3` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`DBItemID`)
) ENGINE=InnoDB AUTO_INCREMENT=46024 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keyboardslot`
--

DROP TABLE IF EXISTS `keyboardslot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `keyboardslot` (
  `OwnerID` int(11) DEFAULT NULL,
  `slotType` tinyint(3) unsigned DEFAULT NULL,
  `isUsed` tinyint(1) DEFAULT NULL,
  `itemType` tinyint(3) unsigned DEFAULT NULL,
  `itemID` smallint(6) DEFAULT NULL,
  `virtualKeyCode` tinyint(3) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keyboardslot`
--

LOCK TABLES `keyboardslot` WRITE;
/*!40000 ALTER TABLE `keyboardslot` DISABLE KEYS */;
/*!40000 ALTER TABLE `keyboardslot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail`
--

DROP TABLE IF EXISTS `mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mail` (
  `OwnerID` int(11) DEFAULT NULL,
  `ID` int(11) DEFAULT NULL,
  `Sender` text,
  `Message` text,
  `TimeStamp` int(11) DEFAULT NULL,
  `IsRead` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail`
--

LOCK TABLES `mail` WRITE;
/*!40000 ALTER TABLE `mail` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `memorystonesavedmap`
--

DROP TABLE IF EXISTS `memorystonesavedmap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `memorystonesavedmap` (
  `IndexInList` tinyint(3) unsigned NOT NULL,
  `MapID` smallint(6) unsigned DEFAULT NULL,
  `OwnerID` int(11) DEFAULT NULL,
  PRIMARY KEY (`IndexInList`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memorystonesavedmap`
--

LOCK TABLES `memorystonesavedmap` WRITE;
/*!40000 ALTER TABLE `memorystonesavedmap` DISABLE KEYS */;
/*!40000 ALTER TABLE `memorystonesavedmap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monstercardset`
--

DROP TABLE IF EXISTS `monstercardset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `monstercardset` (
  `OwnerID` int(11) NOT NULL AUTO_INCREMENT,
  `CardACount` int(11) DEFAULT NULL,
  `CardBCount` int(11) DEFAULT NULL,
  `CardCCount` int(11) DEFAULT NULL,
  `IDForCardA` smallint(6) DEFAULT NULL,
  `MobID` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`OwnerID`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monstercardset`
--

LOCK TABLES `monstercardset` WRITE;
/*!40000 ALTER TABLE `monstercardset` DISABLE KEYS */;
/*!40000 ALTER TABLE `monstercardset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet`
--

DROP TABLE IF EXISTS `pet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pet` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `OwnerID` int(11) DEFAULT NULL,
  `Pet_ID` tinyint(3) unsigned DEFAULT NULL,
  `Growth` tinyint(3) unsigned DEFAULT NULL,
  `Level` tinyint(3) unsigned DEFAULT NULL,
  `Satiety` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pet`
--

LOCK TABLES `pet` WRITE;
/*!40000 ALTER TABLE `pet` DISABLE KEYS */;
/*!40000 ALTER TABLE `pet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `player` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AccountID` int(11) DEFAULT NULL,
  `Name` text,
  `Zed` int(11) DEFAULT NULL,
  `WorldID` int(11) DEFAULT NULL,
  `ByteGender` tinyint(3) unsigned DEFAULT NULL,
  `BytePVPLevel` tinyint(3) unsigned DEFAULT NULL,
  `MapID` smallint(6) DEFAULT NULL,
  `Strength` smallint(6) DEFAULT NULL,
  `Dexterity` smallint(6) DEFAULT NULL,
  `Intelligence` smallint(6) DEFAULT NULL,
  `Luck` smallint(6) DEFAULT NULL,
  `Vitality` smallint(6) DEFAULT NULL,
  `Wisdom` smallint(6) DEFAULT NULL,
  `Bonus_Stats` smallint(6) DEFAULT NULL,
  `Skill_Points` smallint(6) DEFAULT NULL,
  `Hidden` tinyint(1) DEFAULT NULL,
  `Level` tinyint(3) unsigned DEFAULT NULL,
  `HP` smallint(6) unsigned DEFAULT NULL,
  `MP` smallint(6) unsigned DEFAULT NULL,
  `MaxHP` smallint(6) unsigned DEFAULT NULL,
  `MaxMP` smallint(6) unsigned DEFAULT NULL,
  `Attraction` int(11) DEFAULT NULL,
  `Job1` tinyint(3) unsigned DEFAULT NULL,
  `Job2` tinyint(3) unsigned DEFAULT NULL,
  `Job3` tinyint(3) unsigned DEFAULT NULL,
  `Job4` tinyint(3) unsigned DEFAULT NULL,
  `LoginServer_Position` int(11) DEFAULT NULL,
  `Experience` bigint(20) DEFAULT NULL,
  `Equip_Bags` tinyint(3) unsigned DEFAULT NULL,
  `Etc_Bags` tinyint(3) unsigned DEFAULT NULL,
  `Cash_Bags` tinyint(3) unsigned DEFAULT NULL,
  `Guild_ID` int(11) DEFAULT NULL,
  `GuildSignupYear` tinyint(3) unsigned DEFAULT NULL,
  `GuildSignupMonth` tinyint(3) unsigned DEFAULT NULL,
  `GuildSignupDay` tinyint(3) unsigned DEFAULT NULL,
  `GuildNickname` text,
  `MapObjectID` smallint(6) DEFAULT NULL,
  `X` smallint(6) DEFAULT NULL,
  `Y` smallint(6) DEFAULT NULL,
  `ArrowID` smallint(6) DEFAULT NULL,
  `GuildPosition` tinyint(3) unsigned DEFAULT NULL,
  `previousesMapID` smallint(6) DEFAULT NULL,
  `prevX` smallint(6) DEFAULT NULL,
  `prevY` smallint(6) DEFAULT NULL,
  `GSCharacterInfoSent` tinyint(1) DEFAULT NULL,
  `WH_Bags` tinyint(3) unsigned DEFAULT NULL,
  `TradeZed` int(11) DEFAULT NULL,
  `trackPosition` tinyint(1) DEFAULT NULL,
  `QuestT` smallint(6) DEFAULT NULL,
  `Health` int(11) unsigned DEFAULT NULL,
  `Mana` int(11) unsigned DEFAULT NULL,
  `MaxHealth` int(11) unsigned DEFAULT NULL,
  `MaxMana` int(11) unsigned DEFAULT NULL,
  `printPacketHeaders` tinyint(1) DEFAULT NULL,
  `mixerLevel` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player`
--

LOCK TABLES `player` WRITE;
/*!40000 ALTER TABLE `player` DISABLE KEYS */;
/*!40000 ALTER TABLE `player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quest`
--

DROP TABLE IF EXISTS `quest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `quest` (
  `OwnerID` int(11) NOT NULL AUTO_INCREMENT,
  `QuestID` int(11) unsigned NOT NULL,
  `process` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`OwnerID`,`QuestID`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quest`
--

LOCK TABLES `quest` WRITE;
/*!40000 ALTER TABLE `quest` DISABLE KEYS */;
/*!40000 ALTER TABLE `quest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questmob`
--

DROP TABLE IF EXISTS `questmob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `questmob` (
  `PlayerID` int(11) DEFAULT NULL,
  `QuestID` smallint(6) unsigned DEFAULT NULL,
  `monsterID` int(11) unsigned DEFAULT NULL,
  `monsterNR` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questmob`
--

LOCK TABLES `questmob` WRITE;
/*!40000 ALTER TABLE `questmob` DISABLE KEYS */;
/*!40000 ALTER TABLE `questmob` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe`
--

DROP TABLE IF EXISTS `recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recipe` (
  `OwnerID` int(11) NOT NULL AUTO_INCREMENT,
  `Value` smallint(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`OwnerID`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe`
--

LOCK TABLES `recipe` WRITE;
/*!40000 ALTER TABLE `recipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `recipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skill` (
  `OwnerID` int(11) DEFAULT NULL,
  `SID` smallint(6) DEFAULT NULL,
  `Level` tinyint(3) unsigned DEFAULT NULL,
  `cdTimesec` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse`
--

DROP TABLE IF EXISTS `warehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `warehouse` (
  `Zed` int(11) NOT NULL AUTO_INCREMENT,
  `AccountID` int(11) DEFAULT NULL,
  `Bags` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`Zed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouse`
--

LOCK TABLES `warehouse` WRITE;
/*!40000 ALTER TABLE `warehouse` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-03 12:38:13
