apk add openssl

WORK_PATH="/openssl"
FILE="$WORK_PATH/wk3r.cert.pem"
cd $WORK_PATH

if [[ -f "$FILE" ]]; then
    echo "$FILE exists, no need to generate a certificate"
else
    openssl req -config cert.cnf -new -x509 -sha256 -newkey rsa:2048 -nodes -keyout wk3r.key.pem -days 365 -out wk3r.cert.pem
	chmod 644 *.pem
fi